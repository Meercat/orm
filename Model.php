<?php

namespace Model;

class Model
{
    public $table;
    protected $fields     = [];
    protected $properties = [];
    private $db           = null;

    public function __construct()
    {
        $dsn      = "mysql:host=localhost; dbname=orm_test";
        $this->db = new \PDO($dsn, 'root', 'KyivStar010680');

        $this->getTableName();
        $this->getColumnName();
    }

    public function __set($name, $value)
    {
        $this->properties[$name] = $value;
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->fields)) {
            return $this->properties[$name];
        }
    }

    private function getTableName()
    {
        if (array_key_exists('tableName', $this->properties)) {
            $this->table = $this->propertie['tableName'];
            unset($this->propertie);
        } else {
            $className   = strtolower(get_class($this));
            $temp_tbname = str_replace('models\\', '', $className);
            $this->table = strtolower($temp_tbname)."s";
        }
    }

    private function getColumnName()
    {

        foreach ($this->db->query("SHOW COLUMNS FROM ".$this->table) as $column) {
            $this->fields[] = $column['Field'];
        }
        return $this->fields;
    }

    public function save()
    {
        $bindParamNames = [];

        foreach ($this->fields as $field) {
            $bindParamNames[] = ":".$field;
        }

        $fields               = implode(', ', $this->fields);
        $bindParamNamesString = implode(', ', $bindParamNames);

        $stmt = $this->db->prepare("INSERT INTO ".$this->table." (".$fields.") VALUES (".$bindParamNamesString.")");
        foreach ($bindParamNames as $param) {
            $key = str_replace(':', '', $param);
            $stmt->bindParam($param, $this->properties[$key]);
        }
        $stmt->execute();
    }

    public function del($id)
    {
        $stmt = $this->db->query("DELETE FROM ".$this->table." WHERE id=".$id);
    }

    public function update()
    {
        var_dump($this->table);
        foreach ($this->fields as $field) {
            $bindParamNames[] = ":".$field;
        }
        unset($this->fields[0]);
        foreach ($this->fields as $field) {
            $value .= $field."=:".$field.", ";
        }

        $value = substr_replace($value, '', -2);

        $stmt = $this->db->prepare("UPDATE ".$this->table." SET ".$value." WHERE id=:id");
        foreach ($bindParamNames as $param) {
            $key = str_replace(':', '', $param);
            $stmt->bindParam($param, $this->properties[$key]);
        }
        $stmt->execute();
    }
}
?>